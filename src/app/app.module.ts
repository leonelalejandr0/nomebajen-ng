import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { routing } from './app.routing';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatSidenavModule,MatToolbarModule,
  MatIconModule, MatProgressSpinnerModule, MatCardModule, MatInputModule,MatFormFieldModule,
  MatDialogModule, MatSnackBarModule, MatListModule, MatSelectModule} from '@angular/material';

import { AgmCoreModule } from '@agm/core';

//Componentes
import { AppComponent } from './app.component';
import { PanelComponent } from './components/panel/panel.component';
import { MainComponent } from './components/main/main.component';
import { RegistroComponent } from './components/registro/registro.component';
import { LoaderComponent } from './components/loader/loader.component';
import { FiscalizadoresComponent } from './components/fiscalizadores/fiscalizadores.component';
import { BipComponent } from './components/bip/bip.component';
import { BusesComponent } from './components/buses/buses.component';
import { RespSaldoComponent } from './components/resp-saldo/resp-saldo.component';
import { RespBusesComponent } from './components/resp-buses/resp-buses.component';

//Servicios
import { LoaderService } from './services/loader.service';
import { ApiService } from './services/api.service';
import { StorageService } from './services/storage.service';
import { AuthGuardService } from './services/auth-guard.service';
import { DialogFiscalizadorComponent } from './components/dialog-fiscalizador/dialog-fiscalizador.component';
import { DialogAgregarFiscalizadorComponent } from './components/dialog-agregar-fiscalizador/dialog-agregar-fiscalizador.component';
import { ConfigComponent } from './components/config/config.component';




@NgModule({
  declarations: [
    AppComponent,
    PanelComponent,
    MainComponent,
    RegistroComponent,
    LoaderComponent,
    FiscalizadoresComponent,
    BipComponent,
    BusesComponent,
    RespSaldoComponent,
    RespBusesComponent,
    DialogFiscalizadorComponent,
    DialogAgregarFiscalizadorComponent,
    ConfigComponent
  ],
  entryComponents : [
    RegistroComponent,
    RespBusesComponent,
    RespSaldoComponent,
    DialogFiscalizadorComponent,
    DialogAgregarFiscalizadorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    routing,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDwfhmWsr7pOuW2WqOArQDIa-MfSnU_WkE'
    }),
    MatButtonModule, MatCheckboxModule,MatSidenavModule,MatToolbarModule,MatIconModule,
    MatProgressSpinnerModule, MatCardModule, MatInputModule, MatFormFieldModule, MatDialogModule,
    MatSnackBarModule,MatListModule,MatSelectModule
  ],
  providers: [
    LoaderService,
    ApiService,
    StorageService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
