import { Component, OnInit, Inject } from '@angular/core';
import { Router } from "@angular/router";
import { FormControl } from '@angular/forms';

import { RegistroComponent } from '../registro/registro.component';

import {LoaderService} from '../../services/loader.service';
import {ApiService} from '../../services/api.service';
import { StorageService } from '../../services/storage.service';
 
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MatSnackBar} from '@angular/material';

// Declaramos las variables para jQuery
declare var jQuery:any;
declare var $:any;



@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  email:string = "";
  pass:string = "";
  recordar:boolean;
  check:FormControl;

  constructor(private storageService:StorageService, private apiService : ApiService, private router:Router, public dialog: MatDialog,public snackBar: MatSnackBar, private loaderService : LoaderService) {

    this.recordar = this.storageService.recuerdaDatosSesion();
    if(this.recordar){
      this.email = this.storageService.getEmail();
      this.pass = this.storageService.getPassword();
    }


   }

  ngOnInit() {
    if(this.storageService.sesionIniciada()){
      this.router.navigate(['panel']);
    }else{
      setTimeout(this.timer,500);
    }
    

    
  }

  timer(){
    $("#progressBar").fadeOut("slow", function(){
      $("#cardLogin").fadeIn("fast");
    }); 
  }

  ir(url:string){
    this.router.navigate([url]);
  }

  recordarDatos(check){
    if(check.checked){
      
      if(this.validarDatos()){

        this.storageService.recordarDatosSesion("true");
        this.storageService.setEmail(this.email);
        this.storageService.setPassword(this.pass);
      }else{
        check.checked = false;
        
      }
    }else{

      this.storageService.recordarDatosSesion("false");
      this.storageService.setEmail("");
      this.storageService.setPassword("");
      this.email = "";
      this.pass = "";
    }
  }

  btnIngreso(){
    if(this.validarDatos()){
      this.loaderService.loader(true,"Ingreso","Estamos ingresando espera por favor..."); 
      this.apiService.login(this.email,this.pass,this.storageService.getHash())
      .subscribe((res) => {
        console.log(res);
        this.loaderService.loader(false,"","");
         
        if(res.ok){
          this.storageService.setIdUsuario(res.usuario.id);
          this.router.navigate(['panel']);
        }else{
          this.snack(res.msg);
        }
      });
    }
  }

  validarDatos():boolean{
    if(this.email.length){
      let  re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(re.test(this.email)){
        if(this.pass.length){
          return true;
        }else{
          this.snack("Debes ingresar una contraseña");
        }
      }else{
        this.snack("Debes ingresar un email válido");
      }
    }else{
      this.snack("Debes ingresar un email");
    }
    return false;
  }

  snack(str:string){
    this.snackBar.open(str,'', {
      duration: 3000,
    });
  }

  btnRegistro(){
    let dialogRef = this.dialog.open(RegistroComponent, {
      width: '500px',
      data: { name: 'Leonel', animal: 'Perro' }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed ' + result);
      
    });
  }

}




