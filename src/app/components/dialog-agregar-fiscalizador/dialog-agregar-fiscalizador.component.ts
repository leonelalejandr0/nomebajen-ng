import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MatSnackBar} from '@angular/material';
import { ApiService } from '../../services/api.service';
import { LoaderService } from '../../services/loader.service';

// Declaramos las variables para jQuery
declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-dialog-agregar-fiscalizador',
  templateUrl: './dialog-agregar-fiscalizador.component.html',
  styleUrls: ['./dialog-agregar-fiscalizador.component.css']
})
export class DialogAgregarFiscalizadorComponent implements OnInit {

  direccion:string = "";
  descripcion:string = "";
  f:any;

  constructor(private loaderService: LoaderService, public snackBar: MatSnackBar, private apiService:ApiService, public dialogRef: MatDialogRef<DialogAgregarFiscalizadorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log(data);
      this.f = data;
    $('.mat-dialog-container').css("padding",0);
  }

  ngOnInit() {
  }

  agregar(){
    //this.dialogRef.close('Pizza!');
    console.log(this.direccion + " " + this.descripcion);
    if(this.direccion){
      let desc = this.direccion + ' | ' + this.descripcion;
      this.loaderService.loader(true,"Agregar Fiscalización","Estamos agregando la fiscalización, espera por favor...");
      this.apiService.addFiscalizador(this.f.latitud,this.f.longitud,desc)
      .subscribe(res => {
        this.loaderService.loader(false,"","");
        this.snack(res.msg);
        if(res.ok){
          this.dialogRef.close(res.fiscalizador);
        }
      });
    }else{
      this.snack("Debes ingresar la dirección de la fiscalización");
    }
  }

  snack(str:string){
    this.snackBar.open(str,'', {
      duration: 3000,
    });
  }

}
