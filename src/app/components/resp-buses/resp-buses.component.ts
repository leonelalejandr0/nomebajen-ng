import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

// Declaramos las variables para jQuery
declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-resp-buses',
  templateUrl: './resp-buses.component.html',
  styleUrls: ['./resp-buses.component.css']
})
export class RespBusesComponent implements OnInit {

  paradero:any;
  servicios:any = [];

  constructor(public dialogRef: MatDialogRef<RespBusesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    $('.mat-dialog-container').css("padding",0);

      

      
      for(let i = 0; i < data.servicios.length; i++){
        let serv = this.existeServicio(data.servicios[i].servicio);
        if(serv != null){
          let rec;
          serv.valido = data.servicios[i].valido;
          if(data.servicios[i].valido == 1){
            rec = {
              patente : data.servicios[i].patente,
              tiempo : data.servicios[i].tiempo,
              distancia : this.formatearDistancia(data.servicios[i].distancia)
            };
            serv.recorridos.push(rec);
          }else{
            serv.descripcionError = data.servicios[i].descripcionError;
          }
          
        }else{
          let serv = {
            servicio : data.servicios[i].servicio,
            recorridos : [],
            valido : 0,
            descripcionError : ""
          };
          let rec;
          serv.valido = data.servicios[i].valido;
          if(data.servicios[i].valido == 1){
            rec = {
              patente : data.servicios[i].patente,
              tiempo : data.servicios[i].tiempo,
              distancia : this.formatearDistancia(data.servicios[i].distancia)
            };
            serv.recorridos.push(rec);
          }else{
            
              serv.descripcionError = data.servicios[i].descripcionError
            
          }
          
          this.servicios.push(serv);
        }
      }

      this.paradero = data;
      console.log(this.servicios);
      console.log(data);
    //console.log(data);
   }

  ngOnInit() {
  }

  formatearDistancia(mts:string):string{
    let div:any = mts.split(" ");
    let kms:number = div[0] / 1000;
    return (Math.round(kms * 10) / 10) + " kms.";
  }

  existeServicio(serv){
    for(let i = 0; i < this.servicios.length; i++){
      if(this.servicios[i].servicio === serv){
        return this.servicios[i];
      }
    }
    return null;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
