import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../services/loader.service';

import { trigger, style, animate, transition } from '@angular/animations';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-loader',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ opacity: 0}),
          animate('300ms', style({opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('300ms', style({opacity: 0}))
        ])
      ]
    )
  ],
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  isLoading:boolean;
  title:string;
  msg:string;
  subscription : Subscription;



  constructor(private loaderService: LoaderService) {
  }

  ngOnInit() {
      this.subscription = this.loaderService.statusO.subscribe((val: boolean) => {
          this.isLoading = val;
      });

      this.loaderService.titleO.subscribe((val: string) => {
        this.title = val;
      });

      this.loaderService.msgO.subscribe((val: string) => {
        this.msg = val;
      });
  }

}
