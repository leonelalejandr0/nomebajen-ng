import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { LoaderService } from '../../services/loader.service';
import { RespSaldoComponent } from '../resp-saldo/resp-saldo.component';

import {MatSnackBar} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-bip',
  templateUrl: './bip.component.html',
  styleUrls: ['./bip.component.css']
})
export class BipComponent implements OnInit {

  cargando:boolean = true;
  codigo_tarjeta:string = "";
  is_favorito:boolean = false;
  tarjetas:any = [];

  constructor(public dialog: MatDialog,private apiService:ApiService,public snackBar: MatSnackBar, private loaderService: LoaderService) { 
    this.apiService.getTarjetas()
    .subscribe((res) => {
      console.log(res);
      if(res.ok){
        this.tarjetas = res.tarjetasbip;
      }
      this.cargando = false;
    });
  }

  ngOnInit() {
  }

  consultarBip(cod_tarjeta:string){
    if(cod_tarjeta != null && cod_tarjeta != ''){
      if(cod_tarjeta.length <= 8){
        this.loaderService.loader(true,"Consultando Saldo BIP", "Estamos consultando el saldo de la tarjeta, espera por favor...");
        this.apiService.consultarSaldoBip(cod_tarjeta)
        .subscribe(res => {
          if(Object.keys(res).length === 0 && res.constructor === Object){
            this.snack("Ocurrió un problema, intenta nuevamente");
          }else{
            this.mostrarDialog(res);
            this.codigo_tarjeta = "";
          }

          if(this.is_favorito){
            this.apiService.addTarjeta(cod_tarjeta)
            .subscribe((res) => {
              console.log(res);
              if(res.ok){
                let t = {
                  codigo : cod_tarjeta
                };
                this.tarjetas.push(t);
              }
              this.snack(res.msg);
            });
            this.is_favorito = false;
          }

          this.loaderService.loader(false,"","");
          
        });
      }else{
        this.snack("El código no puede superar los 8 carácteres");
      }
    }else{
      this.snack("Debes ingresar el código de la tarjeta");
    }
  }

  snack(str:string){
    this.snackBar.open(str,'', {
      duration: 3000,
    });
  }

  mostrarDialog(data){
    let dialogRef = this.dialog.open(RespSaldoComponent, {
      width: '500px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed ' + result);
      
    });
  }

  eliminarTarjeta(t:any){
    this.apiService.deleteTarjeta(t.codigo)
    .subscribe(res => {
      if(res.ok){
        let i = this.tarjetas.indexOf(t);
        this.tarjetas.splice(i,1);
      }
      this.snack(res.msg);
    });
  }

}
