import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

// Declaramos las variables para jQuery
declare var jQuery:any;
declare var $:any;


@Component({
  selector: 'app-resp-saldo',
  templateUrl: './resp-saldo.component.html',
  styleUrls: ['./resp-saldo.component.css']
})
export class RespSaldoComponent implements OnInit {

  saldo:any;

  constructor(public dialogRef: MatDialogRef<RespSaldoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      this.saldo = data;
      $('.mat-dialog-container').css("padding",0);

    }

  ngOnInit() {
  }

}
