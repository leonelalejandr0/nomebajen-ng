import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { LoaderService } from '../../services/loader.service';
import { RespBusesComponent } from '../resp-buses/resp-buses.component';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-buses',
  templateUrl: './buses.component.html',
  styleUrls: ['./buses.component.css']
})
export class BusesComponent implements OnInit {
  paraderos = [];
  cargando = true;
  is_favorito = false;
  codigo_paradero:string;

  constructor(public dialog: MatDialog,public snackBar: MatSnackBar,private apiService: ApiService, private loaderService: LoaderService) { }

  ngOnInit() {
    this.apiService.getMisParaderos()
    .subscribe((res:any) => {
      console.log(res);
      this.cargando = false;
      if(res.ok){
        this.paraderos = res.paraderos;
      }
    });
  }

  consultarParadero(id:string){
    if(id != null && id != ""){
      console.log(id);
      this.loaderService.loader(true,"Consultando próximos Buses","Estamos consultando los próximos buses, espera por favor...");
      this.apiService.consultarParadero(id)
      .subscribe((res) => {
        this.loaderService.loader(false,"","");
        this.mostrarDialog(res);

        if(this.is_favorito){
          this.apiService.addParadero(id)
          .subscribe((res) => {
            if(res.ok){
              let p = {
                codigo : id
              };
              this.paraderos.push(p);
            }
            this.snack(res.msg);
          });
        }

        this.is_favorito = false;
        this.codigo_paradero = "";

      });
    }else{
      this.snack("Debes ingresar el código del paradero");
    }
  }

  snack(str:string){
    this.snackBar.open(str,'', {
      duration: 3000,
    });
  }

  eliminarParadero(p:any){
    this.apiService.deleteParadero(p.codigo)
    .subscribe(res => {
      if(res.ok){
        let i = this.paraderos.indexOf(p);
        this.paraderos.splice(i,1);
      }
      this.snack(res.msg);
    });
    
  }

  mostrarDialog(paradero){
    let dialogRef = this.dialog.open(RespBusesComponent, {
      width: '500px',
      height : '600px',
      data: paradero
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed ' + result);
      
    });
  }




}
