import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { LoaderService } from '../../services/loader.service';
import  { DialogFiscalizadorComponent } from '../dialog-fiscalizador/dialog-fiscalizador.component';
import  { DialogAgregarFiscalizadorComponent } from '../dialog-agregar-fiscalizador/dialog-agregar-fiscalizador.component';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MatSnackBar} from '@angular/material';


@Component({
  selector: 'app-fiscalizadores',
  templateUrl: './fiscalizadores.component.html',
  styleUrls: ['./fiscalizadores.component.css']
})
export class FiscalizadoresComponent implements OnInit {
  lat: number = -33.4403715;
  lng: number = -70.6499427;
  zoom:number = 9;
  fiscalizadores:any = [];

  constructor(private snackBar : MatSnackBar, public dialog: MatDialog,private apiService:ApiService, private loaderService: LoaderService) { 

    this.apiService.getFiscalizadores()
    .subscribe(res => {
      if(res.ok){
        this.fiscalizadores = res.fiscalizadores;
      }

      if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(position => {
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          this.zoom = 13;
        });
     }
    });

  }

  clickedMarker(f:any){
    let dialogRef = this.dialog.open(DialogFiscalizadorComponent, {
      width: '500px',
      data: f
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed ' + result);
      this.apiService.getFiscalizadores()
      .subscribe(res => {
        if(res.ok){
          this.fiscalizadores = res.fiscalizadores;
        }
      });
    });
  }

  ngOnInit() {
  }

  mapClicked($event) {

    let f = {
      latitud : $event.coords.lat,
      longitud : $event.coords.lng,
      descripcion : ''
    };


    let dialogRef = this.dialog.open(DialogAgregarFiscalizadorComponent, {
      width: '500px',
      data: f
    });

    dialogRef.afterClosed().subscribe(result => {
      this.apiService.getFiscalizadores()
      .subscribe(res => {
        if(res.ok){
          this.fiscalizadores = res.fiscalizadores;
        }
      });
    });
  }

  refrescar(){
    this.loaderService.loader(true,"Actualizando Fiscalizadores", "Estamos actualizando los fiscalizadores, espera por favor...");
    this.apiService.getFiscalizadores()
    .subscribe(res => {
      let cant = 0;
      if(res.ok){
        this.fiscalizadores = res.fiscalizadores;
        cant = this.fiscalizadores.length;
      }
      this.loaderService.loader(false,"","");
      this.snack("Hay " + cant + " fiscalizadores en este momento.");
    });
  }

  snack(str:string){
    this.snackBar.open(str,'', {
      duration: 3000,
    });
  }

}
