import { Component, OnInit, AfterViewInit } from '@angular/core';
import { StorageService } from '../../services/storage.service';
import { Router } from '@angular/router';

// Declaramos las variables para jQuery
declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit, AfterViewInit {

  constructor(private router: Router, private storageService:StorageService) {
   
   }

   ngAfterViewInit(){
    let height_w = $("body").height();
    console.log(height_w);
    let height_t = $("mat-toolbar").height();
    console.log(height_t);
    $(".example-sidenav-content").css('height',height_w - height_t);
   }

  ngOnInit() {
    
  }

  


  cerrarSideNav(sidenav){
    sidenav.close();
  }

  logout(){
    this.storageService.setIdUsuario("");
    this.router.navigate(['']);
  }

}
