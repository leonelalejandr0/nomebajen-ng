import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MatSnackBar} from '@angular/material';
import { ApiService } from '../../services/api.service';
import { StorageService } from '../../services/storage.service';

// Declaramos las variables para jQuery
declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-dialog-fiscalizador',
  templateUrl: './dialog-fiscalizador.component.html',
  styleUrls: ['./dialog-fiscalizador.component.css']
})
export class DialogFiscalizadorComponent implements OnInit {

  fiscalizacion:any;

  constructor(public storageService: StorageService, public snackBar: MatSnackBar, private apiService:ApiService, public dialogRef: MatDialogRef<DialogFiscalizadorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log(data);
      this.fiscalizacion = data;
    $('.mat-dialog-container').css("padding",0);
  }

  ngOnInit() {
  }

  like(id:number){
    this.apiService.likeFiscalizador(id)
    .subscribe(res => {
      if(res.ok){
        if(res.existia){
          this.fiscalizacion.dislikes--;
        }
        this.fiscalizacion.likes++;
        this.snack("Has dado like a este punto");
      }else{
        this.snack(res.msg);
      }
      
    });
  }

  dislike(id:number){
    this.apiService.dislikeFiscalizador(id)
    .subscribe(res => {
      if(res.ok){
        if(res.existia){
          this.fiscalizacion.likes--;
        }
        this.fiscalizacion.dislikes++;
        this.snack("Has dado dislike a este punto");
      }else{
        this.snack(res.msg);
      }
      
    });
  }

  snack(str:string){
    this.snackBar.open(str,'', {
      duration: 3000,
    });
  }

  eliminar(){
    this.apiService.deleteFiscalizador(this.fiscalizacion.id)
    .subscribe(res => {
      this.snack(res.msg);
      this.dialogRef.close();
    });
  }

}
