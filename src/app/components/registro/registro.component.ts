import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';

import { ApiService } from '../../services/api.service';
import { LoaderService } from '../../services/loader.service';

// Declaramos las variables para jQuery
declare var jQuery:any;
declare var $:any;


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls : ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  nombre:string = "";
  email:string = "";
  pass1:string = "";
  pass2:string = "";

  constructor(private loaderService : LoaderService, private apiService : ApiService, private snackBar : MatSnackBar,
    public dialogRef: MatDialogRef<RegistroComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 

      $('.mat-dialog-container').css("padding",0);


    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

  registrar(){
    if(this.nombre){
      if(this.email){
        let  re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(this.email)){
          if(this.pass1){
            if(this.pass2){
              if(this.pass1 == this.pass2){
                this.loaderService.loader(true,"Registro","Estamos registrando tu cuenta, espera por favor...");
                this.apiService.registro(this.nombre,this.email,this.pass1)
                .subscribe(res => {
                  console.log(res);
                  this.snack(res.msg);
                  this.loaderService.loader(false,"","");
                  if(res.ok){
                    this.dialogRef.close();
                  }
                });
              }else{
                this.snack("Las contraseñas no coinciden");
              }
            }else{
              this.snack("Confirma tu contraseña");
            }
          }else{
            this.snack("La contraseña es obligatoria");
          }
        }else{
          this.snack("Ingresa un email válido");
        }
      }else{
        this.snack("El email es obligatorio");
      }
    }else{
      this.snack("El nombre es obligatorio");
    }
  }

  snack(str:string){
    this.snackBar.open(str,'', {
      duration: 3000,
    });
  }

}


