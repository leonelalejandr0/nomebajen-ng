import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent }   from './app.component';
import { PanelComponent } from './components/panel/panel.component';
import { MainComponent } from './components/main/main.component';

import { FiscalizadoresComponent } from './components/fiscalizadores/fiscalizadores.component';
import { BipComponent } from './components/bip/bip.component';
import { BusesComponent } from './components/buses/buses.component';
import { ConfigComponent } from './components/config/config.component';

import {AuthGuardService} from './services/auth-guard.service';


const appRoutes: Routes = [
  {
    path: '',
    component: MainComponent,
    pathMatch: 'full'
  },
  {
      path : 'panel',
      component : PanelComponent,
      canActivate : [AuthGuardService],
      children : [
        {
          path : '',
          redirectTo : 'fiscalizadores',
          pathMatch : 'full',
          canActivate : [AuthGuardService]
        },
        {
          path : 'fiscalizadores',
          component : FiscalizadoresComponent,
          canActivate : [AuthGuardService]
        },
        {
          path: 'bip',
          component : BipComponent,
          canActivate : [AuthGuardService]
        },
        {
          path : 'buses',
          component : BusesComponent,
          canActivate : [AuthGuardService]
        },{
          path : 'configuracion',
          component : ConfigComponent,
          canActivate : [AuthGuardService]
        }
      ]
  },
  {
    path: '**', 
    redirectTo : ''
  }
];

export const routing = RouterModule.forRoot(appRoutes);