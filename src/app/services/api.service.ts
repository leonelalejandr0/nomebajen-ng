import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {StorageService} from './storage.service';
import * as sha1 from 'js-sha1';

@Injectable()
export class ApiService {

  private URL_API:string = "http://200.35.157.166/~nomebajen/api/v1";

  constructor(public http: HttpClient, private storageService:StorageService) { }

  getFiscalizadores(): Observable<any>{
    return this.http.get(this.URL_API+'/Fiscalizadores');
  }

  login(email:string, password:string, device_id:string): Observable<any>{
    let json = {
      device_id : this.storageService.getHash(),
      email : email,
      password : sha1(password),
      tipo_registro : 1
    };
    return this.http.post(this.URL_API+'/InicioSesion', json);
  }

  consultarParadero(id:string):Observable<any>{
    return this.http.get(this.URL_API + '/ConsultarParadero/' + id);
  }

  getMisParaderos(): Observable<any>{
    return this.http.get(this.URL_API + "/VerMisParaderos/" + this.storageService.getIdUsuario());
  }

  addParadero(codigo_paradero:string):Observable<any>{
    let json = {
      id : this.storageService.getIdUsuario(),
      codigo : codigo_paradero
    };
    return this.http.post(this.URL_API+'/AgregarParadero',json);
  }

  getTarjetas():Observable<any>{
    return this.http.get(this.URL_API + "/VerMisTarjetasBIP/" + this.storageService.getIdUsuario());
  }

  consultarSaldoBip(codigo:string):Observable<any>{
    return this.http.get(this.URL_API + '/ConsultarSaldoBIP/' + codigo);
  }

  addTarjeta(codigo:string):Observable<any>{
    let json = {
      id : this.storageService.getIdUsuario(),
      codigo : codigo
    };
    return this.http.post(this.URL_API+'/AgregarTarjeta',json);
  }

  deleteTarjeta(codigo:string):Observable<any>{
    return this.http.get(this.URL_API + '/EliminarTarjeta/' + this.storageService.getIdUsuario() + '/' + codigo);
  }

  deleteParadero(codigo:string):Observable<any>{
    return this.http.get(this.URL_API + '/EliminarParadero/' + this.storageService.getIdUsuario() + '/' + codigo)
  }

  likeFiscalizador(id:number):Observable<any>{
    let json = {
      usuario : this.storageService.getIdUsuario(),
      fiscalizacion : id,
      tipo : 1
    };

    return this.http.post(this.URL_API + '/AgregarLike', json);
  }

  dislikeFiscalizador(id:number):Observable<any>{
    let json = {
      usuario : this.storageService.getIdUsuario(),
      fiscalizacion : id,
      tipo : 0
    };

    return this.http.post(this.URL_API + '/AgregarLike', json);
  }

  addFiscalizador(lat:number,lng:number,descripcion:string):Observable<any>{
    let json = {
      usuario : this.storageService.getIdUsuario(),
      latitud : lat,
      longitud : lng,
      descripcion : descripcion
    };

    return this.http.post(this.URL_API + '/AgregarFiscalizador', json);
  }

  registro(nombre:string,email:string,pass:string):Observable<any>{
    let json = {
      nombre : nombre,
      email : email,
      tipo_registro : 1,
      password : sha1(pass),
      device_id : this.storageService.getHash()
    };
    return this.http.post(this.URL_API + '/RegistroUsuario', json);
  }

  deleteFiscalizador(id:number):Observable<any>{
    return this.http.get(this.URL_API + '/EliminarFiscalizador/'+id+'/'+this.storageService.getIdUsuario());
  }

}
