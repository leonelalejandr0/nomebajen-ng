import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class StorageService {

  constructor(public http: HttpClient) {
    console.log('ejecutar servicio');
    if(!this.existeHash()){
      
      this.setHash(this.makeHash());
    }else{
      console.log("Existe Hash " + this.getHash());
    }

  }

  existeHash():boolean{
    if(localStorage.getItem("hash") === null)
      return false;
    else
      return true;
  }

  getHash():string{
    return localStorage.getItem("hash");
  }

  setHash(hash:string):void{
    localStorage.setItem("hash",hash);
  }

  setEmail(email:string):void{
    localStorage.setItem("email",email);
  }

  setPassword(pass:string):void{
    localStorage.setItem("pass",pass);
  }

  getEmail():string{
    return localStorage.getItem("email");
  }

  getPassword():string{
    return localStorage.getItem("pass");
  }

  recuerdaDatosSesion():boolean{
    if(localStorage.getItem("RecordarDatosSesion") == "true"){
      return true;
    }else{
      return false;
    }
  }

  recordarDatosSesion(recordar):void{
    localStorage.setItem("RecordarDatosSesion",recordar);
  }


  makeHash() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$%&)";
  
    for (var i = 0; i < 15; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  }

  setIdUsuario(id:string){
    sessionStorage.setItem("id",id);
  }

  getIdUsuario():string{
    return sessionStorage.getItem("id");
  }

  sesionIniciada():boolean{
    let id = this.getIdUsuario();
    console.log(id);
    if(typeof id != 'undefined' && id){
      return true;
    }else{
      return false;
    }
  }
  

}
