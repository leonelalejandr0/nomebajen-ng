import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class LoaderService {

  public status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  statusO = this.status.asObservable();
  public title: BehaviorSubject<string> = new BehaviorSubject<string>("Cargando");
  titleO = this.title.asObservable();
  public msg: BehaviorSubject<string> = new BehaviorSubject<string>("");
  msgO = this.msg.asObservable();
  
  loader(value: boolean, title: string, msg : string) {
      this.status.next(value);
      this.title.next(title);
      this.msg.next(msg);

  }



}
