import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { StorageService } from './storage.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private storageService:StorageService, private router : Router) { }

  canActivate():boolean{
    let sesion = this.storageService.sesionIniciada();
    console.log(sesion);
    if(sesion){
      return true;
    }else{
      this.router.navigate(['']);
      return false;
    }
    
    
    
  }

}
